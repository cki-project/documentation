---
title: "Writing documentation"
linktitle: Documentation
description: How to contribute to the documentation
companion: true
weight: 30
aliases: [/l/writing-documentation]
---

New documentation is always welcome!

Start by cloning the documentation repository:

```bash
git clone \
    https://gitlab.com/cki-project/documentation \
    documentation
cd documentation
```

The documentation uses [Hugo] with the [Docsy] theme to build the site,
imported using [Hugo Modules].
All requirements can be installed via the system package manager, but we recommend,
instead, to execute all of the commands in the shell of a container
with everything preinstalled via `make podman`.
In case you really want to install the dependencies, look for
"dependencies for documentation" in the [hugo-docs Dockerfile].

Add new documentation as markdown files inside the appropriate directory
under `content/`. Every file should end in `.md`.

Documentation can be previewed by running `make serve`. A local web server will
start and automatically refresh the preview each time a change is saved.

Check for syntax problems by running `make lint`. If you are dealing with URLs,
check if any of them is broken with `make link-check`.

To submit changes, fork the repository, add the fork as a remote to the
checkout and commit the changes. Push them to a new branch in the fork, and
start a merge request against the parent.

{{% alert title="TL;DR" color="info" %}}

- `make podman`: Start the development container
- `make lint`: Check for errors
- `make link-check`: Check for broken URLs
- `make serve`: Run documentation web server

{{% /alert %}}

## Internal repository

The CKI documentation is **open by default** and maintained via the [public
repository][public-repo]. Additionally, an internal repository
exists to be able to provide further confidential information.

The contents of these two repositories are used to generate two different
versions of the documentation:

- the [public documentation][public-docs], solely based on information from the
  [public repository][public-repo]
- the [internal documentation][internal-docs], based on information from the
  [public repository][public-repo], supplemented by confidential details about
  internal infrastructure from the internal documentation repository.
  This is done using [Hugo Modules] to interleave files from the repositories,
  with files from the internal repository taking precedence.
  This allows the internal repository to add or overwrite files from the public repository.

The internal documentation is redeployed when changes to the public repository are merged.

To contribute to the internal repository, start by cloning the internal
documentation repository **next** to the public repository and connect both
checkouts with a symbolic link:

```bash
git clone \
    https://internal/documentation/repository.git \
    internal-documentation
cd internal-documentation
# Create relative symlink that works both inside `make podman` and outside.
ln -sn "../documentation" public-repo
```

The following step is running the `prepare-public-repo.sh` script,
which will create symbolic links to linting scripts and settings that are
defined in the public repo, but shared among the two documentations.

From this point you have access to every command under `make` mentioned before,
and once again we recommend using a container to execute them.

Note that `make serve` will clean after itself, removing `go.sum` and restoring
`go.mod` to the version from git.

{{% alert title="TL;DR" color="info" %}}

- `./prepare-public-repo.sh`:
- `make podman`: Start the development container
- `make lint`: Check for errors
- `make link-check`: Check for broken URLs
- `make serve`: Run documentation web server

{{% /alert %}}

## Stable incoming links

**None of the page URLs are considered stable.**

To refer to pages from outside the documentation repository:

1. Create a page alias in the front matter like

   ```yaml
   ---
   title: "Writing documentation"
   aliases: [/l/writing-documentation]
   ---
   ```

2. Use this alias to construct the full URL like
   <https://cki-project.org/l/writing-documentation>.

## Adding confidential information

With Hugo, content is organized into [page bundles].

As an example, consider the following structure in the public repository:

```plain
documentation/content/
  \- _index.md
  \- page1.md
  \- page2/
     \- index.md
```

This will generate three pages: an index page for the top-level `_index.md`,
and two content pages for `page1.md` and `page2/index.md`.

To include confidential content from additional Markdown files supplied via the
internal repository:

1. In the public repository, move content Markdown files in branch bundles
   (directories with a `_index.md` file) into separate leaf bundles. In the
   example, this means moving `page1.md` into a separate directory:

   ```plain
   documentation/content/
     \- _index.md
     \- page1/
        \- index.md
     \- page2/
        \- index.md
   ```

1. In the internal repository, add the confidential content as a separate
   Markdown file in the appropriate page bundle:

   ```plain
   internal-documentation/content/
     \- page1/
        \- confidential-section.md
   ```

   This is what `content/page1/confidential-section.md` in the internal
   repository could look like:

   ```plain
   ---
   ---
   <!-- markdownlint-disable first-line-heading -->
   ## Confidential section

   Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
   tempor incididunt ut labore et dolore magna aliqua. ...
   ```

1. In the front matter of `content/page1/index.md` in the public repository,
   mark the page as having a companion page:

   ```plain
   ---
   companion: true
   ---
   ...
   ```

   Include the file with the confidential content with the `include` shortcode:

   ```plain
   ...
   {{%/* include "confidential-section.md" */%}}
   ...
   ```

   When the public documentation is rendered, the missing include is ignored
   gracefully.

{{% include "internal.md" %}}

[public-repo]: https://gitlab.com/cki-project/documentation/

[public-docs]: https://cki-project.org/
[internal-docs]: https://documentation.internal.cki-project.org/

[page bundles]: https://gohugo.io/content-management/page-bundles/

[Hugo]: https://gohugo.io/
[Docsy]: https://www.docsy.dev/
[Hugo Modules]: https://gohugo.io/hugo-modules/
[hugo-docs Dockerfile]: https://gitlab.com/cki-project/containers/-/blob/main/builds/hugo-docs.in
