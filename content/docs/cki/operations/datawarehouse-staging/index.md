---
title: "Syncing the data in the staging instance of DataWarehouse"
linkTitle: "Reset staging DW"
description: >
  How to restore a production backup into the staging instance of DataWarehouse
companion: true
---

## Problem

You want to load fresh data into the staging instance of DataWarehouse.

## Steps

1. Log into the staging OpenShift project on the command line, e.g. by running
   the command on the "Copy login command" page hidden behind the menu in the
   top-right corner of the OpenShift web console.

1. Scale the staging DataWarehouse deployment to zero via `oc scale --replicas 0
   dc/datawarehouse-webservice-staging`.

1. Get the name of one of the finished DataWarehouse backup pods via `oc get
   pod -o name | grep datawarehouse-backup`, and spawn a temporary debug pod
   for it via `oc debug pod/name`.

1. Modify the bucket configuration in the `BUCKET_DH_DW_BACKUPS_STAGING`
   environment variable to point to the production bucket.

1. Run `cki_deployment_pgsql_restore.sh` and select the backup to restore.

1. Wait.

1. Scale the staging DataWarehouse deployment back up via `oc scale --replicas
   2 dc/datawarehouse-webservice-staging`.

{{% include "internal.md" %}}
