---
title: "Mirroring a centos-stream/tests/kernel into the VPN"
linkTitle: "Mirroring tests/kernel repos"
description: >-
  How to set up mirroring of a repository in the centos-stream/tests/kernel
  group into the VPN
companion: true
aliases: [/l/mirror-tests-kernel-repo]
---

{{% include "internal.md" %}}

## Steps

### Public repository

1. Set up a public repository in the
   <https://gitlab.com/redhat/centos-stream/tests/kernel> group.
2. Add an access token:
   - name: `comment`
   - access level: `Guest`
   - scope: `api`

### Internal repository

1. Create a matching internal repository on the internal GitLab instance.
2. Add the CKI service account with Maintainer access.
3. Add an access token:
   - name: `mirror`
   - access level: `Maintainer`
   - scope: `write_repository`

### Deployment repository

1. Take the commit from `gitlab-ci-templates` as a template, and adjust
   repository names and tokens.
2. File a new merge request.
