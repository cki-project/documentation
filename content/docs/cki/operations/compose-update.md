---
title: Updating y-stream composes
linkTitle: Updating y-stream composes
description: How to update y-zstream composes
aliases: [/l/updating-ystream-composes]
---

## Problem

CKI uses a cron job to create an MR to update the nightly composes used when
testing MRs or builds for y-stream kernels. These composes are periodically
removed from Beaker. The cron job runs once per week (Sunday).
This update process is needed to make sure no new issues are introduced
in the compose that would block builds/MRs. Therefore, new non-critical failures
should be added as known issues to [DataWarehouse].

## Steps to review the automated MR

1. Make sure the pipeline passed the `check-kernel-results` job.

1. Unfortunately, even if it passes the `check-kernel-results` job, doesn't mean
there is no problem. Any test that reports `ERROR` or `MISS` will not fail the
`check-kernel-results` job.

    1. Look at each and every test job and make sure all tests ran and didn't
finish with `ERROR` (`FAIL` is fine).

    1. If there was some critical problem (like it was not possible to provision
the compose), a [ServiceNow] ticket for the Beaker team has to be filed.
The current compose needs to be kept until a new compose is available where
these failures are resolved.

    1. If there are new non-critical failures, they should be added to the
[DataWarehouse] with a proper ticket to track them. After creating the regex
wait 30 minutes for the triaging to finish. Re-run the `check-kernel-results`, which should pass now.

1. If everything looks good, approve the MR and merge it.

## y-stream compose configuration

1. To update how the automation selects the y-stream compose, update the [ystream_composes.yaml]
configuration file in [kpet-db].

[kpet-db]: https://gitlab.com/redhat/centos-stream/tests/kernel/kpet-db
[DataWarehouse]: ../../test-maintainers/triager.md
[ServiceNow]: https://redhat.service-now.com/help
[ystream_composes.yaml]: https://gitlab.com/redhat/centos-stream/tests/kernel/kpet-db/-/blob/main/ystream_composes.yaml
