---
title: Adding an expression to the pipeline herder
linkTitle: Retrying jobs via pipeline-herder
description: >
    How to get the pipeline-herder to retry GitLab jobs with certain characteristics
---

## Problem

A GitLab pipeline job failed, and it can be fixed by retrying. This can (and
should!) be done automatically by the [pipeline-herder].

## Steps

1. Determine the job output that is typical for a failed job.

   As an example, job [2612657346] contains the following output:

   ```text
   Downloading artifacts 00:01
   Downloading artifacts for prepare builder (2490531250)...
   ERROR: Downloading artifacts from coordinator... forbidden  id=2490531250 responseStatus=403 Forbidden status=GET https://gitlab.com/api/v4/jobs/2490531250/artifacts: 403 Forbidden token=MUVtKQWe
   FATAL: permission denied
   Uploading artifacts for failed job 00:01
   Uploading artifacts...
   ```

1. Check the raw logs for the relevant line.

   For the example above, the output is

   ```text
   [31;1mERROR: Downloading artifacts from coordinator... forbidden[0;m  [31;1mid[0;m=2490531250 [31;1mresponseStatus[0;m=403 Forbidden [31;1mstatus[0;m=GET https://gitlab.com/api/v4/jobs/2490531250/artifacts: 403 Forbidden [31;1mtoken[0;m=MUVtKQWe
   ```

1. Clone the [cki-tools] repository as described in the [getting started]
   documentation. This will clone the repository, create a fork if necessary,
   setup `direnv` for Python development and install all necessary packages.

   If the repository was cloned already, manually setup a `direnv` Python
   environment and install the dependencies via

   ```bash
   echo 'layout python3' > .envrc
   echo "export GITLAB_TOKENS='{\"gitlab.com\":\"COM_GITLAB_TOKEN_PERSONAL\"}'" >> .envrc
   echo 'export COM_GITLAB_TOKEN_PERSONAL="your-secret-token-from-gitlab-com"' >> .envrc
   direnv allow
   pip install -e .
   ```

1. Create a matcher in the [cki-tools] repository in
   `cki_tools/pipeline_herder/matchers.py`.

   For the example above, such a matcher could look like this:

   ```python
   utils.Matcher(
       name='artifacts-error',
       description='Problem while downloading artifacts',
       messages=re.compile(r'ERROR: Downloading artifacts from coordinator.*forbidden.*403 Forbidden'),
   )
   ```

1. Check that the matcher works by running the pipeline-herder locally on the
   job URL via something like

   ```text
    $ python3 -m cki_tools.pipeline_herder.main --job-url https://gitlab.com/.../-/jobs/2612657346
    Problem while downloading artifacts
    ```

1. Save the tail of the logs with the relevant lines in `tests/pipeline_herder/asserts/traces/`.

1. Add a test case to `tests/pipeline_herder/test_matchers_traces.py`, e.g. something like

   ```python
   def test_artifacts_error_forbidden(self):
       """Test artifacts-error with 403 forbidden error."""
       self._test('artifacts-error', 'artifact_error_forbidden.txt')
   ```

1. File a merge request to get the new matcher deployed.

[getting started]: ../contributing/getting-started.md
[cki-tools]: https://gitlab.com/cki-project/cki-tools
[pipeline-herder]: https://gitlab.com/cki-project/cki-tools/-/tree/main/cki_tools/pipeline-herder
[2612657346]: https://gitlab.com/redhat/red-hat-ci-tools/kernel/cki-internal-pipelines/cki-trusted-contributors/-/jobs/2490531251
