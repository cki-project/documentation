---
title: Adding a kernel tree to CKI building and testing
linkTitle: Adding a kernel tree
description: How to enable testing for a new kernel tree
weight: 70
aliases: [/l/adding-kernel-tree]
---

If you would like to enable testing for your kernel tree, we'll need to know
the following specifics:

* Which kernel tree and branch we should test?
* Which tests should be run?
* Which architectures or specific HW should we test?
* If DataWarehouse dashboard is not enough, where should we send results?
* A [tree name]

Please file an [MR] and we can help you fill in the remaining fields, or open
[an issue]. If you don't have a GitLab account, you can also send an email to
[cki-project@redhat.com] with the details above.

[cki-project@redhat.com]: mailto:cki-project@redhat.com
[MR]: https://gitlab.com/cki-project/pipeline-data/-/merge_requests
[an issue]: https://gitlab.com/cki-project/pipeline-data/-/issues
[tree name]: tree_names.md
