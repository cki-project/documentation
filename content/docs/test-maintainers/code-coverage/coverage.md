---
title: Code coverage
description: Check test coverage using CKI bot
weight: 20
---

CKI provides kernel builds with gcov support and it is possible to trigger CKI
pipelines from merge requests on [kernel-tests] repo to check the coverage of
specific test or tests in general.

## How to trigger using the CKI bot

* Open a dummy MR on [kernel-tests], once the MR is created the CKI bot will
add a comment of the options to trigger the test.
* Under `Click here for details on how to select the tests to run.` there is a
list of kernels that can be used, in this case, look at the `gcov` ones, like
`[gcov/rhel-8]` or `[centos-gcov/c9s]`.
* In the `Click here for details on how to adjust the functionality of the
retriggered pipeline by replacing the trigger variables of the original
pipeline.` section there is a list of options to select the tests or architectures to
run.

## Example how to trigger the bot

```text
@cki-ci-bot please test

- `[skip_beaker=false]`
- `[centos-gcov/c9s]`
- `[architectures=x86_64]`
- `[tests_regex=.*KUNIT.*]`
```

This will create a CKI pipeline that will execute the test.

## Looking the results

Once the pipeline finishes a link to `coverage/report/index.html` will be
shown at the end of the `test` job of the pipeline.

[kernel-tests]: https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests
